----------------------------------------------------------------------------------------------------------------------------
INSTALLATION
----------------------------------------------------------------------------------------------------------------------------
1) Backup your skin files

2) Move the CONTENT of the .zip file to the skin directory: C:\Users\[USER]\AppData\Roaming\XBMC\addons\skin.aeon.nox.gotham\
Important! The skin modifies "includes.xml" file. If you have done modifications to this file, update it manualy (keep reading below), otherwise just overwrite all.



Updating "includes.xml" manualy:
a) Open the "1080i" folder and locate the file "includes.xml"

b) Add the following line after line 40 (or after "Includes_Videoinfo" line):
		<include file="Includes_LiveTV.xml" />
		
c) Save, close & enjoy. 



----------------------------------------------------------------------------------------------------------------------------
CHANGELOG
----------------------------------------------------------------------------------------------------------------------------
Version 0.8
- New: Added a new EPG-timeline sideblade option for channel display (Name, Icon or Name & icon - see screenshots in first post). Usefull for those who use "bar-icons". 
- New: Added minified video player in EPG timeline view (visible when you press back while in fullscreen)
- Fixed error in the EPG timeline where one could not exit EPG while watching a channel. Pressing backspace, while in epg, will now work as "back". 

Version 0.7
- New sideblade option for the mod: Extended Channel OSD (lets you switch between standard and extended channel osd)
- Updated mod to work with the git version
- Fixed error with seekbar and full screen info while playing local content
- Fixed error with custom\colored flags on local content (hopefully)

Version 0.6
- Rollback on "backspace to open sideblade". Back to normal setting.
- Removed pvr-addon label on infobar. Bigger fonts on channel number and name.
- Fixed AM/PM label issue on infobar & main channel view
- Added new selected-channel texture in Channel-OSD
- EPG Timeline: Removed 5min markers
- EPG Timeline: 2h timeblocks

Version 0.5
- Fixed navigation-bug in all tv-views.

Version 0.4
- Replaced the topbar to be the same one used in all other areas\views (now with weather icons and item-count info)
- EPG Timeline: Fixed aspect ratio bug on channel logos
- EPG Timeline: Improved epg-genre color usage (It will now first try to fetch epg-color by "Genre Type", if that fails it will then try with "Genre Name", if that fails it will fall-back to transparent light-grey color called "0.png")
- EPG Timeline: Added new focus texture (transparent with thin border, uses color from appearance settings) and colored font. 

Version 0.3
- Fixed minor color-bugs
- Removed GPU label in infobar 
- Main Channels list (smallist) now more similar to the one in Channels OSD
- Improved EPG Timeline view (2,5h display, added time-marker, better focus image, genre-color support...)
- EPG Timeline genre colors: Create your own genre colors in the skin folder (skin.aeon.nox.gotham\media\new_pvr\epg-genres\). Name them by genre name: Action.png, Comedy.png, Sports.png...
- PS! Backspace is now the main button to enter the "left-menu". Use Escape to exit the view. 

Version 0.2
- Fixed the duration of the infobar display after switching channel (you change the durration in settings).
- Fixed error in displaying description of a programme in ChannelOSD
- Removed the seekbar at the bottom when switching channels

Version 0.1
- Modified only VideoOSD, SeekbarOSD, ChannelOSD, FullScreenInfoOSD
- Added animation to infobar and osd
- Added support for AM\PM time-labels
- Added more consistent (better looking) media-flag icons
- Progressbars and labels use color-settings from the skin
- Added support for pvr-addons that use the <icon src="..." /> tag from XMLTV files, saved in "PlotOutline" db-field 
- Tested on "IPTV Simple" pvr-addon